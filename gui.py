from src.grammar import *
from src.automata import *
from src.grammar_automata import *
from src.regex import *
from src.validate import *

from PyQt5.QtWidgets import QMainWindow, QTextEdit, QAction, QFileDialog, QApplication, QGridLayout, QWidget, QPushButton, QMessageBox
from PyQt5.QtGui import QIcon
import sys, os

class AppWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.currentType = "empty"
        self.current = None
        self.initUI()
        
    def initUI(self):
        # self.textEdit.setStyleSheet("font: 30pt; color: red;")
        self.textEdit = QTextEdit()
        self.textEdit.setReadOnly(True)
        self.textEdit.setFontFamily("Consolas, Source Code Pro, Courier")
        
        self.textEdit.setText("Open a file to start")

        self.initAutomataMenu()
        self.initGrammarMenu()
        self.initRegexMenu()

        self.setCentralWidget(self.textEdit)
        
        self.statusBar()

        openGrammar = QAction('Otwórz gramatykę', self)
        openGrammar.setShortcut('Ctrl+G')
        openGrammar.setStatusTip('Otwórz plik z gramatyką')
        openGrammar.triggered.connect(self.showGrammarDialog)

        openAuto = QAction('Otwórz automat', self)
        openAuto.setShortcut('Ctrl+A')
        openAuto.setStatusTip('Otwórz plik z automatem')
        openAuto.triggered.connect(self.showAutomataDialog)

        openRegex = QAction('Otwórz wyrażenie regularne', self)
        openRegex.setShortcut('Ctrl+R')
        openRegex.setStatusTip('Otwórz plik z wyrażeniem regularnym')
        openRegex.triggered.connect(self.showRegexDialog)

        saveAction = QAction('Zapisz jako...', self)
        saveAction.setShortcut('Ctrl+S')
        saveAction.setStatusTip('Zapisz aktualnie widoczny obiekt')
        saveAction.triggered.connect(self.saveDialog)

        menubar = self.menuBar()
        fileMenu = menubar.addMenu('&Plik')
        fileMenu.addAction(openGrammar)
        fileMenu.addAction(openAuto)
        fileMenu.addAction(openRegex)
        fileMenu.addAction(saveAction)
        
        self.setGeometry(300, 300, 800, 600)
        self.setWindowTitle('Kombajn')
        self.show()

    def saveDialog(self):
        fname = QFileDialog.getSaveFileName(self, 'Save file', os.curdir)
        if fname[0]:
            try:
                if self.currentType == 'regex':
                    with open(fname[0], 'w') as f:
                        f.write(self.current)
                elif self.currentType == 'enfa' or self.currentType == 'nfa' or self.currentType == 'dfa':
                    save_to_csv(self.current, fname[0])
                else:
                    save_grammar(self.current, fname[0])
            except:
                QMessageBox.critical(self, "Błąd", "Nie można zapisać pliku.")

    
    def initAutomataMenu(self):
        self.autoText = QTextEdit()
        self.autoText.setReadOnly(True)
        self.autoText.setFontFamily("Consolas, Source Code Pro, Courier")

        grid = QGridLayout()
        self.autoWidget = QWidget()
        self.autoWidget.setLayout(grid)
        grid.addWidget(self.autoText, 0, 1, 5, 1)
        buttonGrammar = QPushButton("Konwertuj na gramatykę")
        buttonGrammar.clicked.connect(self.__automataToGrammar)
        grid.addWidget(buttonGrammar, 0, 0)
        buttonRegex = QPushButton("Konwertuj na wyrażenie regularne")
        buttonRegex.clicked.connect(self.__automataToRegex)
        grid.addWidget(buttonRegex, 1, 0)
        buttonNfa = QPushButton("Konwertuj na NAS")
        buttonNfa.clicked.connect(self.__automataToNfa)
        grid.addWidget(buttonNfa, 2, 0)
        buttonDfa = QPushButton("Konwertuj na DAS")
        buttonDfa.clicked.connect(self.__automataToDfa)
        grid.addWidget(buttonDfa, 3, 0)
        buttonMinDfa = QPushButton("Konwertuj na minimalny DAS")
        buttonMinDfa.clicked.connect(self.__automataToDfaMin)
        grid.addWidget(buttonMinDfa, 4, 0)
    
    def initGrammarMenu(self):
        self.grammarText = QTextEdit()
        self.grammarText.setReadOnly(True)
        self.grammarText.setFontFamily("Consolas, Source Code Pro, Courier")
        grid = QGridLayout()
        self.grammarWidget = QWidget()
        self.grammarWidget.setLayout(grid)
        grid.addWidget(self.grammarText, 0, 1, 7, 1)

        buttonEps = QPushButton("Uprość gramatykę")
        buttonEps.clicked.connect(self.__grammarSimplify)
        grid.addWidget(buttonEps, 0, 0)

        buttonRight = QPushButton("Konwertuj na gramatykę prawostronną")
        buttonRight.clicked.connect(self.__grammarToRight)
        grid.addWidget(buttonRight, 1, 0)

        buttonRegex = QPushButton("Konwertuj na wyrażenie reguarne")
        buttonRegex.clicked.connect(self.__grammarToRegex)
        grid.addWidget(buttonRegex, 2, 0)

        buttonENAS = QPushButton("Konwertuj na e-NAS")
        buttonENAS.clicked.connect(self.__grammarToEnfa)
        grid.addWidget(buttonENAS, 3, 0)

        buttonNAS = QPushButton("Konwertuj na NAS")
        buttonNAS.clicked.connect(self.__grammarToNfa)
        grid.addWidget(buttonNAS, 4, 0)

        buttonDAS = QPushButton("Konwertuj na DAS")
        buttonDAS.clicked.connect(self.__grammarToDfa)
        grid.addWidget(buttonDAS, 5, 0)

        buttonMinDAS = QPushButton("Konwertuj na minimalny DAS")
        buttonMinDAS.clicked.connect(self.__grammarToDfaMin)
        grid.addWidget(buttonMinDAS, 6, 0)
    
    def initRegexMenu(self):
        self.regexText = QTextEdit()
        self.regexText.setReadOnly(True)
        self.regexText.setFontFamily("Consolas, Source Code Pro, Courier")

        grid = QGridLayout()
        self.regexWidget = QWidget()
        self.regexWidget.setLayout(grid)
        grid.addWidget(self.regexText, 0, 1, 5, 1)
        buttonGrammar = QPushButton("Konwertuj na gramatykę")
        buttonGrammar.clicked.connect(self.__regexToGrammar)
        grid.addWidget(buttonGrammar, 0, 0)
        buttonEnfa = QPushButton("Konwertuj na e-NAS")
        buttonEnfa.clicked.connect(self.__regexToEnfa)
        grid.addWidget(buttonEnfa, 1, 0)
        buttonNfa = QPushButton("Konwertuj na NAS")
        buttonNfa.clicked.connect(self.__regexToNfa)
        grid.addWidget(buttonNfa, 2, 0)
        buttonDfa = QPushButton("Konwertuj na DAS")
        buttonDfa.clicked.connect(self.__regexToDfa)
        grid.addWidget(buttonDfa, 3, 0)
        buttonMinDfa = QPushButton("Konwertuj na minimalny DAS")
        buttonMinDfa.clicked.connect(self.__regexToDfaMin)
        grid.addWidget(buttonMinDfa, 4, 0)
        
    def showGrammarDialog(self):
        fname = QFileDialog.getOpenFileName(self, 'Open file', os.curdir)
        if fname[0]:
            try:
                gr = load_from_file(fname[0])
                if not is_lh_grammar(gr) and not is_rh_grammar(gr):
                    QMessageBox.critical(self, "Błąd", "Gramatyka nie jest regularna")
                else:
                    try:
                        self.setCentralWidget(self.grammarWidget)
                    except:
                        self.initGrammarMenu()
                        self.setCentralWidget(self.grammarWidget)
                    self.grammarText.setText(str(gr))
                    self.current = gr
                    self.currentType = "grammar"
            except:
                QMessageBox.critical(self, "Błąd", "Nie można załadować pliku.")

    def showRegexDialog(self):
        fname = QFileDialog.getOpenFileName(self, 'Open file', os.curdir)
        if fname[0]:
            try:
                with open(fname[0], 'r') as f:
                    regex = f.read()
                if not is_regex(regex):
                    QMessageBox.critical(self, "Błąd", 'Niepoprawne wyrażenie regularne')
                else:
                    try:
                        self.setCentralWidget(self.regexWidget)
                    except:
                        self.initRegexMenu()
                        self.setCentralWidget(self.regexWidget)
                    self.regexText.setText(regex)
                    self.current = regex
                    self.currentType = "regex"
            except:
                QMessageBox.critical(self, "Błąd", "Nie można załadować pliku.")

    def showAutomataDialog(self):
        fname = QFileDialog.getOpenFileName(self, 'Open file', os.curdir)
        if fname[0]:
            try:
                auto = load_from_csv(fname[0])
                isDfa, auto = try_as_dfa(auto)
                is_ok = True
                if isDfa:
                    autoType = "dfa"
                else:
                    isNfa, auto = try_as_nfa(auto)
                    if isNfa:
                        autoType = "nfa"
                    else:
                        isEnfa, auto = try_as_enfa(auto)
                        if isEnfa:
                            autoType = "enfa"
                        else:
                            is_ok = False
                if not is_ok:
                    QMessageBox.critical(self, "Błąd", "Niepoprawny automat")
                else:
                    try:
                        self.setCentralWidget(self.autoWidget)
                    except:
                        self.initAutomataMenu()
                        self.setCentralWidget(self.autoWidget)
                    self.autoText.setText(auto.to_string(na_rep=''))
                    self.current = auto
                    self.currentType = autoType 
            except:
                QMessageBox.critical(self, "Błąd", "Nie można załadować pliku.")
    
    def __grammarToRight(self):
        try:
            if is_rh_grammar(self.current):
                QMessageBox.information(self, "Informacja", "Gramatyka jest już prawostronna.")
            else:
                tmp = self.current
                self.current = left_to_right(tmp)
                self.currentType = "grammar"
                self.grammarText.setText(str(self.current))
        except:
            QMessageBox.critical(self, "Błąd", "Konwersja nie powiodła się.")

    
    def __grammarSimplify(self):
        try:
            self.current = simplify_grammar(self.current)
            self.grammarText.setText(str(self.current))
        except:
            QMessageBox.critical(self, "Błąd", "Konwersja nie powiodła się.")

    def __grammarToNfa(self):
        try:
            tmp = grammar_to_enfa(self.current)
            self.current = enfa_to_nfa(tmp)
            try:
                self.setCentralWidget(self.autoWidget)
            except:
                self.initAutomataMenu()
                self.setCentralWidget(self.autoWidget)
            self.currentType = "nfa"
            self.autoText.setText(self.current.to_string())
        except:
            QMessageBox.critical(self, "Błąd", "Konwersja nie powiodła się.")
    
    def __grammarToEnfa(self):
        try:
            self.current = grammar_to_enfa(self.current)
            try:
                self.setCentralWidget(self.autoWidget)
            except:
                self.initAutomataMenu()
                self.setCentralWidget(self.autoWidget)
            self.currentType = "enfa"
            self.autoText.setText(self.current.to_string())
        except:
            QMessageBox.critical(self, "Błąd", "Konwersja nie powiodła się.")
    
    def __grammarToDfa(self):
        try:
            tmp = grammar_to_enfa(self.current)
            tmp = enfa_to_nfa(tmp)
            self.current = nfa_to_dfa(tmp)
            try:
                self.setCentralWidget(self.autoWidget)
            except:
                self.initAutomataMenu()
                self.setCentralWidget(self.autoWidget)
            self.currentType = "dfa"
            self.autoText.setText(self.current.to_string())
        except:
            QMessageBox.critical(self, "Błąd", "Konwersja nie powiodła się.")

    def __grammarToDfaMin(self):
        try:
            tmp = grammar_to_enfa(self.current)
            tmp = enfa_to_nfa(tmp)
            tmp = nfa_to_dfa(tmp)
            self.current = minimize_dfa(tmp)
            try:
                self.setCentralWidget(self.autoWidget)
            except:
                self.initAutomataMenu()
                self.setCentralWidget(self.autoWidget)
            self.currentType = "dfa"
            self.autoText.setText(self.current.to_string())
        except:
            QMessageBox.critical(self, "Błąd", "Konwersja nie powiodła się.")
    
    def __grammarToRegex(self):
        try:
            enfa = grammar_to_enfa(self.current)
            self.current = automata_to_regex(enfa)
            try:
                self.setCentralWidget(self.regexWidget)
            except:
                self.initRegexMenu()
                self.setCentralWidget(self.regexWidget)
            self.currentType = "regex"
            self.regexText.setText(self.current)
        except:
            QMessageBox.critical(self, "Błąd", "Konwersja nie powiodła się.")
    
    def __automataToNfa(self):
        try:
            if self.currentType == 'dfa' or self.currentType == 'nfa':
                QMessageBox.information(self, "Informacja", "Automat już jest pozbawiony epislon-przejść.")
                return

            self.current = enfa_to_nfa(self.current)
            self.currentType = "nfa"
            self.autoText.setText(self.current.to_string())
        except:
            QMessageBox.critical(self, "Błąd", "Konwersja nie powiodła się.")
        
    def __automataToDfa(self):
        try:
            if self.currentType == 'dfa':
                QMessageBox.information(self, "Informacja", "Automat już jest deterministyczny.")
                return

            tmp = self.current
            if self.currentType == 'enfa':
                tmp = enfa_to_nfa(tmp)
            self.current = nfa_to_dfa(tmp)
            self.currentType = "dfa"
            self.autoText.setText(self.current.to_string())
        except:
            QMessageBox.critical(self, "Błąd", "Konwersja nie powiodła się.")
        
    def __automataToDfaMin(self):
        try:
            tmp = self.current
            if self.currentType == 'enfa':
                tmp = enfa_to_nfa(tmp)
                tmp = nfa_to_dfa(tmp)
            elif self.currentType == 'nfa':
                tmp = nfa_to_dfa(tmp)
            self.current = minimize_dfa(tmp)
            self.currentType = "dfa"
            self.autoText.setText(self.current.to_string())
        except:
            QMessageBox.critical(self, "Błąd", "Konwersja nie powiodła się.")
        
    def __automataToGrammar(self):
        try:
            self.current = fa_to_grammar(self.current)
            try:
                self.setCentralWidget(self.grammarWidget)
            except:
                self.initGrammarMenu()
                self.setCentralWidget(self.grammarWidget)
            self.currentType = "grammar"
            self.grammarText.setText(str(self.current))        
        except:
            QMessageBox.critical(self, "Błąd", "Konwersja nie powiodła się.")
        
    def __automataToRegex(self):
        try:
            self.current = automata_to_regex(self.current)
            try:
                self.setCentralWidget(self.regexWidget)
            except:
                self.initRegexMenu()
                self.setCentralWidget(self.regexWidget)
            self.currentType = "regex"
            self.regexText.setText(self.current)
        except:
            QMessageBox.critical(self, "Błąd", "Konwersja nie powiodła się.")
    
    def __regexToEnfa(self):
        try:
            self.current = regex_to_enfa(self.current)
            try:
                self.setCentralWidget(self.autoWidget)
            except:
                self.initAutomataMenu()
                self.setCentralWidget(self.autoWidget)
            self.currentType = "enfa"
            self.autoText.setText(self.current.to_string())
        except:
            QMessageBox.critical(self, "Błąd", "Konwersja nie powiodła się.")
    
    def __regexToNfa(self):
        try:
            tmp = regex_to_enfa(self.current)
            self.current = enfa_to_nfa(tmp)
            try:
                self.setCentralWidget(self.autoWidget)
            except:
                self.initAutomataMenu()
                self.setCentralWidget(self.autoWidget)
            self.currentType = "nfa"
            self.autoText.setText(self.current.to_string())
        except:
            QMessageBox.critical(self, "Błąd", "Konwersja nie powiodła się.")
        
    def __regexToDfa(self):
        try:
            tmp = regex_to_enfa(self.current)
            tmp = enfa_to_nfa(tmp)
            self.current = nfa_to_dfa(tmp)
            try:
                self.setCentralWidget(self.autoWidget)
            except:
                self.initAutomataMenu()
                self.setCentralWidget(self.autoWidget)
            self.currentType = "dfa"
            self.autoText.setText(self.current.to_string())
        except:
            QMessageBox.critical(self, "Błąd", "Konwersja nie powiodła się.")
        
    def __regexToDfaMin(self):
        try:
            tmp = regex_to_enfa(self.current)
            tmp = enfa_to_nfa(tmp)
            tmp = nfa_to_dfa(tmp)
            self.current = minimize_dfa(tmp)
            try:
                self.setCentralWidget(self.autoWidget)
            except:
                self.initAutomataMenu()
                self.setCentralWidget(self.autoWidget)
            self.currentType = "dfa"
            self.autoText.setText(self.current.to_string())
        except:
            QMessageBox.critical(self, "Błąd", "Konwersja nie powiodła się.")
        
    def __regexToGrammar(self):
        try:
            tmp = regex_to_enfa(self.current)
            self.current = fa_to_grammar(tmp)
            try:
                self.setCentralWidget(self.grammarWidget)
            except:
                self.initGrammarMenu()
                self.setCentralWidget(self.grammarWidget)
            self.currentType = "grammar" # more info needed
            self.grammarText.setText(str(self.current))
        except:
            QMessageBox.critical(self, "Błąd", "Konwersja nie powiodła się.")
        
if __name__ == '__main__':
    
    app = QApplication(sys.argv)
    ex = AppWindow()
    app.exec_()