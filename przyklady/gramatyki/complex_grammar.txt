{
	"terminal": ["a","b","c","d"],
	"non-terminal": ["S", "A", "B", "C", "D", "E", "F"],
	"starting": "S",
	"productions": [ "S->aA|bB|cC", "A->aA|dE", "B->bB|dE|eps", "C->cC|dE", "D->a|eps|A|B", "E->d|dF|eps", "F->aF|bF|cF"]
}