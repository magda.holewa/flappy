from .utils import *

import pandas as pd

# TODO poprawic gramatyki zeby przyjmowaly pojedyncze produkcje

def is_lh_grammar(grammar):
    terminals = grammar.terminal
    non_terminals = grammar.nonterminal
    productions = grammar.productions
    start = grammar.starting
   
    if len(set(terminals).intersection(set(non_terminals))) != 0:
        return False
   
    if start not in non_terminals:
        return False
   
    for production in productions:
        if production.lhs not in non_terminals:
            return False
        rhs = production.rhs
        if len(rhs) == 1 and rhs[0] not in terminals and rhs[0] not in non_terminals and rhs[0] != 'eps':
            return False
        if len(rhs) == 2 and not (rhs[0] in non_terminals and rhs[1] in terminals):
            return False
        if len(rhs) > 2 or len(rhs) < 1:
            return False
    return True
 
def is_rh_grammar(grammar):
    terminals = grammar.terminal
    non_terminals = grammar.nonterminal
    productions = grammar.productions
    start = grammar.starting
   
    if len(set(terminals).intersection(set(non_terminals))) != 0:
        return False
   
    if start not in non_terminals:
        return False
   
    for production in productions:
        if production.lhs not in non_terminals:
            return False
        rhs = production.rhs
        if len(rhs) == 1 and rhs[0] not in terminals and rhs[0] not in non_terminals and rhs[0] != 'eps':
            return False
        if len(rhs) == 2 and not (rhs[0] in terminals and rhs[1] in non_terminals):
            return False
        if len(rhs) > 2 or len(rhs) < 1:
            return False
    return True
 
def is_eps_grammar(grammar):
    productions = grammar.productions
    start = grammar.starting
   
    for production in productions:
        if production.lhs == start:
            continue
        if production.rhs[0] == 'eps':
            return True
    return False
 
def is_dfa(automata):
    if 'eps' in automata.columns:
        print("Has eps column")
        return False
    states = automata.index
   
    for transition in automata.columns:
        if transition == 'type':
            continue
        for index, row in automata.iterrows():
            if type(automata[transition][index]) != str:
                print("Has a non str value")
                return False
            s = automata[transition][index]
            if (not is_valid_nonterminal(s)) or s not in states:
                print("State "+s+" not valid")
                return False
    return True
       
def is_nfa(automata):
    if 'eps' in automata.columns:
        print("Has eps column")
        return False
    states = automata.index
    for transition in automata.columns:
        if transition == 'type':
            continue
        for index, row in automata.iterrows():
            if type(automata[transition][index]) != set:
                print("has a non set value")
                return False
            s = automata[transition][index]
            for state in s:
                if (not is_valid_nonterminal(state)) or state not in states:
                    print("state " + state + " not valid")
                    return False
    return True
 
def is_enfa(automata):
    if 'eps' not in automata.columns:
        print("Lacks eps column")
        return False
    states = automata.index
    for transition in automata.columns:
        if transition == 'type':
            continue
        for index, row in automata.iterrows():
            if type(automata[transition][index]) != set:
                print("Has a non set value")
                return False
            s = automata[transition][index]
            for state in s:
                if (not is_valid_nonterminal(state)) or state not in states:
                    print("State "+state+" not valid")
                    return False
    return True
 
def try_as_dfa(automata):
    if is_dfa(automata):
        return True, automata
   
    if is_enfa(automata):
        enfa = automata.copy()
        for s in enfa['eps']:
            if len(s) > 0:
                return False, automata
        enfa = enfa.drop('eps', axis='columns')
        for t in enfa.columns:
            if t == 'type':
                continue
            for i, _ in enfa.iterrows():
                if len(enfa[t][i]) == 1:
                    value = ""
                    for v in enfa[t][i]:
                        value = v
                    enfa[t][i] = value
                else:
                    return False, automata
        return True, enfa
    elif is_nfa(automata):
        nfa = automata.copy()
        for t in nfa.columns:
            if t == 'type':
                continue
            for i, r in nfa.iterrows():
                if len(nfa[t][i]) == 1:
                    value = ""
                    for v in nfa[t][i]:
                        value = v
                    nfa[t][i] = value
                else:
                    return False, automata
        return True, nfa
    else:
        return False, automata
       
def try_as_nfa(automata):
    if is_nfa(automata):
        return True, automata
    if is_dfa(automata):
        nfa = automata.copy()
        for t in nfa.columns:
            for i, _ in nfa.iterrows():
                nfa[t][i] = {nfa[t][i]}
        return True, nfa
    elif is_enfa(automata):
        enfa = automata.copy()
        for s in enfa['eps']:
            if len(s) > 0:
                return False, automata
        enfa = enfa.drop('eps', axis='columns')
        return True, enfa
    else:
        return False, automata
       
def try_as_enfa(automata):
    if is_enfa(automata):
        return True, automata
    if is_nfa(automata):
        nfa = automata.copy()
        nfa = nfa.assign(eps = pd.Series([""]*len(nfa['type'])).values)
        for i, _ in nfa.iterrows():
            nfa['eps'][i] = set()
        return True, nfa
    elif is_dfa(automata):
        dfa = automata.copy()
        dfa = nfa.assign(eps = pd.Series([""]*len(dfa['type'])).values)
        for i, r in dfa.iterrows():
            dfa['eps'][i] = set()
        for t in dfa.columns:
            if t == 'eps' or t == 'type':
                continue
            for i, r in dfa.iterrows():
                dfa[t][i] = {dfa[t][i]}
        return True, dfa
    else:
        return False, automata
 
def is_regex(regex):
    letters = [chr(i) for i in range(ord('a'), ord('z')+1)]
    numbers = [chr(i) for i in range(ord('0'), ord('9')+1)]

    expected = ['('] + letters
    parentheses_open = 0
    for ch in regex:
        if ch not in expected:
            return False
        
        if ch in letters or ch in numbers:
            expected = ['(', ')', '|', '*'] + letters + numbers
        elif ch == "*":
            expected = ['(', ')', '|', '*'] + letters
        elif ch == "|":
            expected = ['('] + letters
        elif ch == "(":
            expected = ['('] + letters
            parentheses_open += 1
        elif ch == ")":
            expected = ['(', ')', '|', '*'] + letters
            parentheses_open -= 1
            if parentheses_open < 0:
                return False
        else:
            return False
    if parentheses_open != 0:
        return False

    return True