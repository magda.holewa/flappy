import re
import json
from .utils import generate_nonterminal, is_valid_terminal, is_valid_nonterminal


class Production(object):
    def __init__(self, lhs, rhs):
        self.lhs = lhs
        self.rhs = rhs
    
    def __str__(self):
        out = ""
        out += self.lhs
        out += '->'
        for i in self.rhs:
            out += i
        return out


class Grammar(object):
    def __init__(self):
        self.terminal = []
        self.nonterminal = []
        self.productions = []
        self.starting = None

    def __str__(self):
        out = ""
        out += "Symbole terminalne:\n" + str(self.terminal)
        out += "\nSymbole nieterminalne:\n" + str(self.nonterminal)
        out += "\nSymbol startowy: " + str(self.starting)
        out += "\nProdukcje\n"
        for p in self.productions:
            out += str(p)
            out += "\n"
        return out


def load_productions(prod, gr):
    grammar_type = ""
    for p in prod:
        sides = p.split('->')
        lhs = sides[0]
        rhs = sides[1]
        if lhs in gr.nonterminal:
            rhs_elements = rhs.split('|')
            for r in rhs_elements:
                if r == 'eps':
                    # type A->eps
                    pr = Production(lhs, [r])
                    gr.productions.append(pr)
                else:
                    res = re.findall(pattern=r'[a-zA-Z][0-9]*', string=r)
                    # type A->a
                    if len(res) == 1:
                        # A->a
                        if res[0] in gr.terminal:
                            pr = Production(lhs, res)
                            gr.productions.append(pr)
                        elif res[0] in gr.nonterminal:
                            pr = Production(lhs, res)
                            gr.productions.append(pr)
                        else:
                            raise ValueError("wrong production syntax")
                    elif len(res) == 2:
                        if res[0] in gr.nonterminal and res[1] in gr.terminal:
                            # Left-sided production
                            if grammar_type == "right":
                                raise ValueError("two types of production in one grammar")
                            else:
                                grammar_type = "left"
                                pr = Production(lhs, res)
                                gr.productions.append(pr)
                        elif res[0] in gr.terminal and res[1] in gr.nonterminal:
                            # right-sided production
                            if grammar_type == "left":
                                raise ValueError("two types of production in one grammar")
                            else:
                                grammar_type = "right"
                                pr = Production(lhs, res)
                                gr.productions.append(pr)
                        else:
                            raise ValueError("wrong production RHS")
        else:
            raise ValueError("wrong production syntax")


def load_from_dict(grammar):
    gr = Grammar()
    gr.terminal = [t for t in grammar['terminal'] if is_valid_terminal(t)]
    gr.nonterminal = [t for t in grammar['non-terminal'] if is_valid_nonterminal(t)]
    gr.starting = grammar['starting'] if is_valid_nonterminal(grammar['starting']) else gr.nonterminal[0]
    load_productions(grammar['productions'], gr)
    return gr


def load_from_file(filePath):
    with open(filePath, 'r') as fp:
        obj = json.load(fp)
    return load_from_dict(obj)


def save_grammar(grammar, filePath):
    gr = dict()
    gr['terminal'] = grammar.terminal
    gr['non-terminal'] = grammar.nonterminal
    gr['starting'] = grammar.starting
    gr['productions'] = [str(p) for p in grammar.productions]
    with open(filePath, 'w') as fp:
        json.dump(gr, fp)

def left_to_right(left_grammar):
    right_grammar = Grammar()
    right_grammar.terminal = left_grammar.terminal
    right_grammar.nonterminal = left_grammar.nonterminal

    productions = left_grammar.productions

    # check if we need to add starting symbol
    add_starting = False
    for p in productions:
        if left_grammar.starting in p.rhs:
            add_starting = True
            break

    if add_starting:
        new_starting = generate_nonterminal(right_grammar.nonterminal, 'S')
        right_grammar.nonterminal.append(new_starting)
        right_grammar.starting = new_starting
        productions.append(Production(new_starting, [left_grammar.starting, 'eps']))
    else:
        right_grammar.starting = left_grammar.starting

    for p in productions:
        if p.lhs == right_grammar.starting:
            if len(p.rhs) == 2:
                # case: S->Aa
                right_grammar.productions.append(Production(p.rhs[0], [p.rhs[1]]))
            elif p.rhs[0] in right_grammar.terminal:
                right_grammar.productions.append(Production(p.lhs, [p.rhs[0]]))
            elif p.rhs[0] in right_grammar.nonterminal:
                right_grammar.productions.append(Production(p.rhs[0], ['eps']))
        elif len(p.rhs) == 1:
            # A->a
            right_grammar.productions.append(Production(right_grammar.starting, [p.rhs[0], p.lhs]))
        elif len(p.rhs) == 2:
            # A->Ba
            right_grammar.productions.append(Production(p.rhs[0], [p.rhs[1], p.lhs]))
        else:
            raise ValueError()
    return right_grammar


def reduce_grammar(grammar):
    g = grammar
    nonterminal = set()
    productions = set()
    starting = g.starting
    
    endpoints = g.terminal + ['eps']
    producing_symbols = set()

    for prod in g.productions:
        if len(prod.rhs) == 1 and (prod.rhs[0] in g.terminal or prod.rhs[0] == 'eps'):
            producing_symbols.add(prod.lhs)

    print(producing_symbols)
    
    oldLen = len(producing_symbols)
    while True:
        for prod in g.productions:
            should_add = True
            for r in prod.rhs:
                if r not in g.terminal and r not in producing_symbols and r != 'eps':
                    should_add = False
                    break
            if should_add:
                producing_symbols.add(prod.lhs)      
        print(producing_symbols)
        if len(producing_symbols) == oldLen:
            break
        oldLen = len(producing_symbols)
    
    print("====")

    for prod in g.productions:
        if prod.lhs in producing_symbols:
            add = True
            for r in prod.rhs:
                if r not in producing_symbols and r not in g.terminal and r != 'eps':
                    add = False
                    break
            if add:
                productions.add(prod)
    
    for prod in productions:
        print(prod.lhs+"->"+str(prod.rhs))
    print("====")
    g.nonterminal = list(producing_symbols)
    g.productions = list(productions)
    
    nonterminal = {g.starting}
    terminal = set()
    productions = set()
    
    oldLen = len(nonterminal)
    while True:
        for prod in g.productions:
            if prod.lhs in nonterminal:
                productions.add(prod)
                for r in prod.rhs:
                    if r in g.nonterminal:
                        nonterminal.add(r)
                    elif r in g.terminal:
                        terminal.add(r)
        if len(nonterminal) == oldLen:
            break
        oldLen = len(nonterminal)

    for prod in productions:
        print(prod.lhs+"->"+str(prod.rhs))
    print("====")

    g.terminal = list(terminal)
    g.nonterminal = list(nonterminal)
    g.productions = list(productions)
    
    return g

def remove_unit_productions(grammar):
    g = grammar
    
    unit_productions = []
    productions = set(g.productions)
    
    for p in g.productions:
        if len(p.rhs) == 1 and p.rhs[0] in g.nonterminal:
            unit_productions.append(p)
        
    for p in unit_productions:
        print(p.lhs+"->"+str(p.rhs))
        if p.lhs == p.rhs[0]:
            productions.discard(p)
            continue
        
        for prod in g.productions:
            if prod.lhs == p.rhs[0]:
                new_prod = Production(p.lhs, prod.rhs)
                productions.add(new_prod)
                if len(new_prod.rhs) == 1 and new_prod.rhs[0] in g.nonterminal:
                    unit_productions.append(new_prod)
        productions.discard(p)
        
    g.productions = list(productions)
    return g
            
def remove_epsilon_productions(grammar):
    nullable_states = set()
    g = grammar
    
    for p in g.productions:
        if len(p.rhs) == 1 and p.rhs[0] == 'eps':
            nullable_states.add(p.lhs)
    
    for nullable in nullable_states:
        for p in g.productions:
            to_add = set()
            for i in range(0,len(p.rhs)):
                if p.rhs[i] == nullable:
                    n = p.rhs[:i]+p.rhs[i+1:]
                    
                    new_prod = Production(p.lhs,n) if len(n) > 0 else Production(p.lhs,['eps'])
                    
                    already_added = False
                    for old_prod in to_add:
                        if new_prod.lhs == old_prod.lhs and new_prod.rhs == old_prod.rhs:
                            already_added = True
                    for old_prod in g.productions:
                        if new_prod.lhs == old_prod.lhs and new_prod.rhs == old_prod.rhs:
                            already_added = True
                            
                    if not already_added:
                        to_add.add(new_prod)
            g.productions += list(to_add)
    
    to_remove = set()
    for prod in g.productions:
        if len(prod.rhs) == 1 and prod.rhs[0] == 'eps':
            to_remove.add(prod)
    for prod in to_remove:
        g.productions.remove(prod)
    return g
    
def simplify_grammar(grammar):
    ret = remove_unit_productions(grammar)
    ret = reduce_grammar(ret)
    ret = remove_epsilon_productions(ret)
    ret = remove_unit_productions(grammar)
    ret = reduce_grammar(ret)
    return ret