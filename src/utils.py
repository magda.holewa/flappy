import re


def generate_nonterminal(existing_symbols, first_letter='Q'):
    suffixes = [int(s[1:]) for s in existing_symbols if s[0] == first_letter and len(s) > 1]
    suffixes.append(-1)
    return first_letter + str(max(suffixes)+1)


def is_valid_terminal(symbol):
    if symbol == 'eps':
        return True
    pattern = re.compile(r'[a-z][0-9]*')
    return not (pattern.fullmatch(symbol) is None)


def is_valid_nonterminal(symbol):
    pattern = re.compile(r'[A-Z][0-9]*')
    return not (pattern.fullmatch(symbol) is None)


def str_to_set(x):
    test = str(x)
    if test == 'nan':
        return set()
    test = test[1:-1]
    test = test.split(",")
    for i in range(0, len(test)):
        test[i] = test[i].strip()
        test[i] = test[i].strip("'")
    test = set(test)
    return test


def set_to_str(s):
    if len(s) < 1:
        return "{}"
    arr = [t for t in s]
    arr.sort()
    ret = "{"
    for v in arr:
        ret = ret + str(v)
        ret = ret + ", "
    ret = ret[:-2]
    ret = ret + "}"
    return ret
