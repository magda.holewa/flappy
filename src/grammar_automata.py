from .utils import generate_nonterminal
from .grammar import Grammar, Production, left_to_right
from .validate import is_lh_grammar
import pandas as pd


def grammar_to_enfa(grammar):
    if is_lh_grammar(grammar):
        grammar = left_to_right(grammar)
    final_state = generate_nonterminal(grammar.nonterminal, 'F')
    enas = pd.DataFrame([], columns=grammar.terminal + ['eps', 'type'], index=grammar.nonterminal + [final_state])
    enas['type'] = pd.Series('', index=enas.index)
    enas['type'][grammar.starting] = '>'
    enas['type'][final_state] = '*'
    for transition in enas.columns:
        if transition == 'type':
            continue
        for index, _ in enas.iterrows():
            enas[transition][index] = set()

    for p in grammar.productions:
        if len(p.rhs) == 1:
            idx = p.rhs[0]
            if p.rhs[0] in grammar.nonterminal:
                if isinstance(enas['eps'][p.lhs], set):
                    enas['eps'][p.lhs].add(p.rhs[0])
                else:
                    enas['eps'][p.lhs] = {p.rhs[0]}
            else:
                if isinstance(enas[idx][p.lhs], set):
                    enas[idx][p.lhs].add(final_state)
                else:
                    enas[idx][p.lhs] = {final_state}
        else:
            if isinstance(enas[p.rhs[0]][p.lhs], set):
                enas[p.rhs[0]][p.lhs].add(p.rhs[1])
            else:
                enas[p.rhs[0]][p.lhs] = {p.rhs[1]}
    return enas


def fa_to_grammar(enas):
    grammar = Grammar()
    for transition in enas.columns:
        if transition == 'type' or transition == 'eps':
            continue
        grammar.terminal.append(transition)
    transitionless = set()
    for index, row in enas.iterrows():
        hasAnyTransition = False
        for transition in enas.columns:
            if transition == 'type':
                continue
            result = enas[transition][index]
            if result == result:  # is not NaN
                hasAnyTransition = True
                break
        if not hasAnyTransition:
            transitionless = transitionless | {index}
            continue
        grammar.nonterminal.append(index)

    for index, row in enas.iterrows():
        if index in transitionless:
            continue
        if enas['type'][index] == ">" or enas['type'][index] == ">*":
            grammar.starting = index
            if enas['type'][index] == ">*":
                grammar.productions.append(Production(index, ['eps']))
        for transition in enas.columns:
            if transition == "type":
                continue
            result = enas[transition][index]
            if isinstance(result, set):
                for r in result:
                    if r not in transitionless:
                        grammar.productions.append(Production(index, [transition, r]))
                    if enas['type'][r] == "*" or enas['type'][r] == ">*":
                        grammar.productions.append(Production(index, [transition]))
            elif result == result:
                if result not in transitionless:
                    grammar.productions.append(Production(index, [transition, result]))
                if enas['type'][result] == "*" or enas['type'][result] == ">*":
                    grammar.productions.append(Production(index, [transition]))

    for p in grammar.productions:
        if len(p.rhs) == 2:
            if p.rhs[0] == 'eps':
                p.rhs = [p.rhs[1]]
            elif p.rhs[1] == 'eps':
                p.rhs = [p.rhs[0]]
    return grammar
