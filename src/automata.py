from .utils import str_to_set, set_to_str, generate_nonterminal
from collections import deque
from itertools import chain, combinations
import numpy as np
import pandas as pd
from .validate import *


def load_from_csv(filePath):
    df = pd.read_csv(filePath, index_col=0)
    file_type = ""
    for c in df.columns:
        if c == "type":
            continue
        for index, row in df.iterrows():
            s = str(df[c][index])
            if s == "nan":
                continue
            if s[0] == "{" and s[-1] == '}':
                if file_type == "dfa":
                    raise ValueError("Ambigious automata format")
                else:
                    file_type = "nfa"
            else:
                if file_type == "nfa":
                    raise ValueError("Ambigious automata format")
                else:
                    file_type = "dfa"
    if file_type == "nfa":
        for c in df.columns:
            if c == "type":
                continue
            df[c] = df[c].apply(str_to_set)

    for i, _ in df.iterrows():
        if str(df['type'][i]) == "nan":
            df['type'][i] = ""
    
    return df


def save_to_csv(fa, filePath):
    def replaceEmptySet(item):
        if type(item) == set and len(item) == 0:
            return ""
        else:
            return item
    df = fa.copy()
    for c in df.columns:
        if c == "type":
            continue
        df[c] = df[c].apply(replaceEmptySet)
    df.to_csv(filePath, index=True, header=True)

def get_starting_symbol(nas):
    for index, row in nas.iterrows():
        if row["type"] == ">" or row["type"] == ">*":
            return index
    return ""


# Finds the epsilon closure of a nfa node
# Arguments are:
#   enas - The e-nfa analized
#   index - The index for which to find the closure
#   alreadyInClosure - prevents infinite loops by not checking the same index again
def closure(enas, index, alreadyInClosure=None):
    if not alreadyInClosure:
        alreadyInClosure = set()
    # Closure of a node contains that node
    cl = {index}
    # Get the possible transitions from the node
    row = enas.loc[index]
    # Is there any epsilon transition from this node?
    if(isinstance(row['eps'], set)):
        # If yes, add it to the closure
        cl = cl | row['eps']
        # For every element that we just added, we must also add it's closure
        for e in row['eps']:
            # Only check for the closure if we have not done that in some previous recursion
            if e not in alreadyInClosure:
                # Find the closure and add it to the current
                next_cl = closure(enas, e, alreadyInClosure | cl)
                cl = cl | next_cl
    return cl


# Converts an e-nfa to a nfa
def enfa_to_nfa(enas):
    nas = enas.copy()
    # Is there a need to convert?
    ok, a = try_as_nfa(enas)
    if ok:
        return a

    start = get_starting_symbol(enas)
    start_cl = closure(nas, start)
    for s in start_cl:
        if nas['type'][s] == "*":
            nas['type'][start] = ">*"
    #For every node in the graph
    for index, row in nas.iterrows():
        # Find the closure of this node
        cl = closure(enas, index)
        # Update the columns with transitions that are possible from every node in the closure
        for transition in nas.columns:
            # Do not udate epsilon transitions as this colum will be removed anyway
            if transition == 'eps' or transition == 'type':
                continue
            # For every graph node that is in the closure
            for node_in_closure in cl:
                # Does the node from our closure have a transition that we want?
                if(isinstance(nas[transition][node_in_closure], set)):
                    # If it has, then we need to add the node reachable from the transition
                    # And its closure because we can go from one node to another like this:
                    #    S ---epsilon---> A ---a---> B ---epsilon---> C
                    toAdd = set()
                    for node_after_transition in nas[transition][node_in_closure]:
                        toAdd = toAdd | closure(enas, node_after_transition)
                    # Now we add everything that we reached to the original transition
                    # or if there was none, we simply put it there
                    if(isinstance(row[transition],set)):
                        row[transition] = row[transition] | toAdd
                    else:
                        row[transition] = toAdd
    # The last step is to remove the obsolete epsilon productions
    nas = nas.drop('eps', axis='columns')
    return nas


def is_state_type(string):
    return string == "" or string == ">" or string == "*" or string == ">*"


def add_state_type(stateType, stateToAdd):
    if not is_state_type(stateType) or not is_state_type(stateToAdd):
        return
    if stateType == "":
        return stateToAdd
    if stateToAdd == "":
        return stateType
    if stateType == stateToAdd:
        return stateType
    return ">*"

def remove_unreachable_states(in_das):
    das = in_das.copy()
    das = das.assign(reachable=np.zeros(len(das["type"])))
    queue = deque([get_starting_symbol(das)])
    das["reachable"][queue[0]] = 1

    while not len(queue) == 0:
        toCheck = queue.popleft()
        for transition in das.columns:
            if transition == "type" or transition == "reachable":
                continue
            toAdd = das[transition][toCheck]
            if(type(toAdd) == set):
                toAdd = str(set_to_str(toAdd))
            if das["reachable"][toAdd] == 0:
                das["reachable"][toAdd] = 1
                queue.append(toAdd)

    das = das.loc[das.reachable > 0]
    das = das.drop("reachable", axis="columns")
    return das


def generate_subsets(states):
    s = list(states)
    return map(set, chain.from_iterable(combinations(s, r) for r in range(len(s)+1)))


def rename(toRename):
    das = toRename.copy()
    mapping = dict()
    existing_names = []
    for index, row in das.iterrows():
        new_name = generate_nonterminal(existing_names)
        mapping[index] = new_name
        existing_names.append(new_name)
    for index, row in das.iterrows():
        for transition in das.columns:
            if transition == 'type':
                continue
            if len(das[transition][index]) > 0:
                das[transition][index] = mapping[das[transition][index]]
    indexes = das.index.values
    for i in range(len(indexes)):
        indexes[i] = mapping[indexes[i]]
    return das


def nfa_to_dfa_slow(nas):

    ok, a = try_as_dfa(nas)
    if ok:
        return a

    rowNames = set()
    for index, row in nas.iterrows():
        rowNames.add(index)
    subsets = generate_subsets(rowNames)
    startingSymbol = get_starting_symbol(nas)
    das = pd.DataFrame([], columns=nas.columns, index=[])
    for sset in subsets:
        row = pd.Series(name=set_to_str(sset))
        for transition in nas.columns:
            if transition == "type":
                continue
            reachableStates = set()
            for state in sset:
                if(isinstance(nas[transition][state], set)):
                    reachableStates = reachableStates | nas[transition][state]
            row[transition] = set_to_str(reachableStates)
        row["type"] = ""
        if len(sset) == 1:
            for v in sset:
                if v == startingSymbol:
                    row["type"] = nas['type'][v]
        for state in sset:
            stateType = nas["type"][state]
            if stateType == "*":
                row["type"] = add_state_type(row["type"], stateType)
                break
        das = das.append(row)
    das = remove_unreachable_states(das)
    das = rename(das)
    return das

def nfa_to_dfa(nas):
    ok, a = try_as_dfa(nas)
    if ok:
        return a

    startingSymbol = {get_starting_symbol(nas)}
    das = pd.DataFrame([], columns=nas.columns, index=[])
    
    reached = [startingSymbol]
    toAdd = deque([startingSymbol])
    
    while len(toAdd) != 0:
        toCheck = toAdd.popleft()
        toCheck_str = set_to_str(toCheck)
        
        row = pd.Series(name=toCheck_str)
        
        for transition in nas.columns:
            if transition == 'type':
                continue
            reachableStates = set()
            for state in toCheck:
                reachableStates |= nas[transition][state]
            if reachableStates not in reached:
                toAdd.append(reachableStates)
                reached.append(reachableStates)
            row[transition] = set_to_str(reachableStates)
        row['type'] = ""
        if toCheck == startingSymbol:
            row['type'] = ">"
            
        for state in toCheck:
            if nas['type'][state] == "*" or nas['type'][state] == ">*":
                if row['type'] == ">":
                    row['type'] = ">*"
                else:
                    row['type'] = "*" 
            
        das = das.append(row)
    das = rename(das)
    return das

def is_equivalent(pair, das, equivalent_pairs):
    if len(pair) == 1:
        return True
    for transition in das.columns:
        if transition == "type":
            continue
        pair_list = list(pair)
        A = pair_list[0]
        B = pair_list[1]
        fromA = das[transition][A]
        fromB = das[transition][B]
        if set([fromA, fromB]) not in equivalent_pairs:
            return False
    return True


def minimize_dfa(toMinimize):
    das = remove_unreachable_states(toMinimize)
    equivalent_pairs = []
    indexes = das.index
    indexes = sorted(indexes)
    for index1 in indexes:
        stan1 = das["type"][index1]
        for index2 in indexes:
            stan2 = das["type"][index2]
            if ((stan1 == "" or stan1 == ">") and (stan2 == "" or stan2 == ">")) or ((stan1 == "*" or stan1 == ">*") and (stan2 == "*" or stan2 == ">*")):
                equivalent_pairs.append(set([index1, index2]))
    oldLen = len(equivalent_pairs) + 1
    while oldLen != len(equivalent_pairs):
        oldLen = len(equivalent_pairs)
        equivalent_pairs = [p for p in equivalent_pairs if is_equivalent(p, das, equivalent_pairs)]
    equivalent_pairs = [p for p in equivalent_pairs if len(p) > 1]
    unique = []
    [unique.append(pair) for pair in equivalent_pairs if pair not in unique]
    to_drop = set()
    for pair in unique:
        pair_list = list(pair)
        A = pair_list[0] if pair_list[0] < pair_list[1] else pair_list[1]
        B = pair_list[1] if pair_list[0] < pair_list[1] else pair_list[0]

        if das['type'][A] == ">" or das['type'][A] == ">*":
            if das['type'][B] == "*":
                das['type'][B] = ">*"
            else:
                das['type'][B] = ">"
        elif das['type'][B] == ">" or das['type'][B] == ">*":
            if das['type'][A] == "*":
                das['type'][A] = ">*"
            else:
                das['type'][A] = ">"

        for transition in das.columns:
            if transition == "type":
                continue
            for index, _ in das.iterrows():
                if das[transition][index] == B:
                    das[transition][index] = A
        to_drop = to_drop | {B}
    for B in to_drop:
        das = das.drop(B, axis="rows")
    return das
