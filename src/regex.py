from .utils import generate_nonterminal, is_valid_nonterminal, is_valid_terminal
import pandas as pd

    
def getSymbols(regex):
    specialChars = set(['(', ')', '|', '*'])
    symbols = set()
    curr = ""
    for ch in regex:
        if ch not in specialChars:
            if ch == 'e' and curr == '':
                curr = ch
                continue
            if curr == 'e':
                if ch == 'p':
                    curr = "ep"
                    continue
                else:
                    symbols.add('e')
                    curr = ch
            elif curr == "ep":
                if ch == 's':
                    curr = ""
                    symbols.add("eps")
                    continue
                else:
                    curr = ch
                    symbols.add('e')
                    symbols.add('p')
            

            if curr == "":
                curr = ch
            else:
                if ch >= '0' and ch <= '9':
                    curr = curr + ch
                else:
                    symbols.add(curr)
                    curr = ch
        else:
            if curr != "":
                symbols.add(curr)
    if curr != "":
        symbols.add(curr)
    
    return symbols

def strip_parentheses(regex):
    if regex[0] != '(' or regex[-1] != ')':
        return regex
    par_count = 0
    for i in range(0, len(regex)):
        ch = regex[i]
        if ch == '(':
            par_count += 1
        if ch == ')':
            par_count -= 1
        if par_count == 0:
            if i == len(regex)-1:
                return regex[1:-1]
            else:
                return regex
    return regex

def split_regex_by_and(regex):
    splitted = []
    current = ""
    parentesis_count = 0
    
    next_to_check = 0
    for i in range(0,len(regex)):
        if i < next_to_check:
            continue
        next_to_check = i+1

        ch = regex[i]
        if ch == '(':
            parentesis_count += 1
        if ch == ')':
            parentesis_count -= 1
        
        current = current+ch
        symbol_complete = False
        if i == len(regex)-1:
            symbol_complete = True
        elif not (regex[i+1] >= '0' and regex[i+1] <= '9'):
            symbol_complete = True
        
        if i < len(regex)-2:
            if current == 'e' and regex[i+1] == 'p' and regex[i+2] =='s':
                current = 'eps'
                symbol_complete = True
                next_to_check = i+3

        if parentesis_count == 0 and len(current) > 0 and symbol_complete:
            splitted.append(current)
            current = ""
    return splitted
      
def split_regex_by_or(regex):
    splitted = []
    current = ""
    parentesis_count = 0
    for ch in regex:
        if ch == '(':
            parentesis_count += 1
        if ch == ')':
            parentesis_count -= 1
        
        if parentesis_count == 0 and ch == '|':
            splitted.append(current)
            current = ""
        else:
            current = current+ch
    if len(current) > 0:
        splitted.append(current)
    return splitted

def generateSimpleENAS(symbol, symbols, existing_nonterminals):
    q0 = generate_nonterminal(existing_nonterminals)
    existing_nonterminals.add(q0)
    q1 = generate_nonterminal(existing_nonterminals)
    existing_nonterminals.add(q1)
    c = list(symbols)
    if 'eps' not in c:
        c = c+['eps']
    c = c+['type']
    enas = pd.DataFrame([], columns=c, index=[q0,q1])
    enas['type'][q0] = '>'
    enas['type'][q1] = '*'
    for transition in enas.columns:
        if transition == 'type':
            continue
        for index, row in enas.iterrows():
            enas[transition][index] = set()
    enas[symbol][q0] |= {q1}
    return enas

def __join_by_or(automata1, automata2, existing_nonterminals):
    joined = pd.concat([automata1, automata2])
    starts = []
    ends = []
    for index, row in joined.iterrows():
        if joined['type'][index] == ">" or joined['type'][index] == ">*":
            starts.append(index)
            joined['type'][index] = ""
        if joined['type'][index] == "*" or joined['type'][index] == ">*":
            ends.append(index)
            joined['type'][index] = ""
    q0 = generate_nonterminal(existing_nonterminals)
    existing_nonterminals.add(q0)
    q1 = generate_nonterminal(existing_nonterminals)
    existing_nonterminals.add(q1)
    data = [""] * len(joined.columns)
    row0 = pd.Series(data, index=joined.columns, name=q0)
    row1 = pd.Series(data, index=joined.columns, name=q1)
    joined = joined.append(row0)
    joined = joined.append(row1)
    
    for transition in joined.columns:
        if transition == 'type':
            joined[transition][q0] = ">"
            joined[transition][q1] = "*"
        else:
            joined[transition][q0] = set()
            joined[transition][q1] = set()
    joined['eps'][q0] |= set(starts)
    for end in ends:
        joined['eps'][end] |= {q1}
    return joined

def join_by_or(automatas, existing_nonterminals):
    if len(automatas) == 1:
        return automatas[0]
    elif len(automatas) == 2:
        return __join_by_or(automatas[0], automatas[1], existing_nonterminals)
    else:
        joined = [__join_by_or(automatas[0],automatas[1], existing_nonterminals)]
        return join_by_or(joined+automatas[2:], existing_nonterminals)

def __multiply(automata, existing_nonterminals):
    starts = set()
    ends = set()
    for index, row in automata.iterrows():
        if automata['type'][index] == ">" or automata['type'][index] == ">*":
            starts.add(index)
            automata['type'][index] = ""
        if automata['type'][index] == "*" or automata['type'][index] == ">*":
            ends.add(index)
            automata['type'][index] = ""
    q0 = generate_nonterminal(existing_nonterminals)
    existing_nonterminals.add(q0)
    q1 = generate_nonterminal(existing_nonterminals)
    existing_nonterminals.add(q1)
    data = [""] * len(automata.columns)
    row0 = pd.Series(data, index=automata.columns, name=q0)
    row1 = pd.Series(data, index=automata.columns, name=q1)
    automata = automata.append(row0)
    automata = automata.append(row1)
    for transition in automata.columns:
        if transition == 'type':
            automata[transition][q0] = ">"
            automata[transition][q1] = "*"
        else:
            automata[transition][q0] = set()
            automata[transition][q1] = set()
    automata['eps'][q0] |= (starts | {q1})
    for end in ends:
        automata['eps'][end] |= (starts | {q1})
    return automata
    
def __join_by_and(automata1, automata2, existing_nonterminals):
    ends = set()
    for index, row in automata1.iterrows():
        if automata1['type'][index] == "*" or automata1['type'][index] == ">*":
            ends |= {index}
            automata1['type'][index] = ""
    starts = set()
    for index, row in automata2.iterrows():
        if automata2['type'][index] == ">" or automata2['type'][index] == ">*":
            starts |= {index}
            automata2['type'][index] = ""
    joined = pd.concat([automata1, automata2])
    for end in ends:
        joined['eps'][end] |= starts
    return joined
    
def join_by_and(automatas, existing_nonterminals):
    if len(automatas) == 1:
        return automatas[0]
    elif len(automatas) == 2:
        return __join_by_and(automatas[0], automatas[1], existing_nonterminals)
    else:
        joined = [__join_by_and(automatas[0],automatas[1], existing_nonterminals)]
        return join_by_and(joined+automatas[2:], existing_nonterminals)

def generateENAS(regex, symbols, existing_nonterminals):
    while True:
        before_strip = regex
        regex = strip_parentheses(regex)
        if regex == before_strip:
            break
    
    splitted = split_regex_by_or(regex)
    if len(splitted) > 1:
        automatas = []
        for split in splitted:
            automata = generateENAS(split, symbols, existing_nonterminals)
            automatas.append(automata)
        return join_by_or(automatas, existing_nonterminals)
    else:
        splitted = split_regex_by_and(regex)
        
        if len(splitted) > 1:
            automatas = []
            for split in splitted:
                if(split != "*"):
                    automatas.append(generateENAS(split, symbols, existing_nonterminals))
                else:
                    automatas[-1] = __multiply(automatas[-1], existing_nonterminals)
            return join_by_and(automatas, existing_nonterminals)
        else:
            if regex in symbols:
                # regex is subregex. For regex ab* it will call generateSimpleENAS("a", ['a', 'b'])
                #                                           and generateSimpleENAS("b", ['a', 'b'])
                return generateSimpleENAS(regex, symbols, existing_nonterminals)
            elif regex == "*":
                return "*"
            else:
                raise ValueError("Wrong regex format")
    
def regex_to_enfa(regex):
    symbols = getSymbols(regex)
    
    enas = generateENAS(regex, symbols, set())
    return enas



def __get_transitions(automata, q0, q1):
    transitions = []
    for transition in automata.columns:
        if transition == 'type':
            continue
        if q1 in automata[transition][q0]:
            transitions.append(transition)
    return transitions

def __concat_regex(strings):
    strings = set(strings)
    if len(strings) == 0:
        return ""
    if len(strings) == 1:
        for string in strings:
            return string
    out = "("
    first = True
    for string in strings:
        if not first:
            out += "|"
        first = False
        out += string
    out += ")"
    return out

def __is_single_symbol(string):
    return is_valid_terminal(string)

def __remove_state(automata):
    indexToRemove = ""
    for index, row in automata.iterrows():
        if(automata['type'][index] == ""):
            indexToRemove = index
            break
    
    sources = set()
    destinations = set()
    selfRecurring = False
    
    for index, row in automata.iterrows():
        for transition in automata.columns:
            if transition == 'type':
                continue
            for i in automata[transition][index]:
                if i == indexToRemove:
                    if index == indexToRemove:
                        selfRecurring = True
                    else:
                        sources |= {index}
    for transition in automata.columns:
        if transition == 'type':
            continue
        for i in automata[transition][indexToRemove]:
            if i != indexToRemove:
                destinations |= {i}
    
    if(len(destinations) > 0):
        for source in sources:
            string_prefix = __concat_regex(__get_transitions(automata, source, indexToRemove))
            
            if selfRecurring:
                s = __concat_regex(__get_transitions(automata, indexToRemove, indexToRemove))
                if s != 'eps':
                    if not __is_single_symbol(s):
                        s = "("+strip_parentheses(s)+")*"
                    else:
                        s = strip_parentheses(s)+"*"
                    if string_prefix == 'eps':
                        string_prefix = s
                    else:
                        string_prefix += s
            
            for destination in destinations:
                string_sufix = __concat_regex(__get_transitions(automata, indexToRemove, destination))
                if string_prefix == 'eps' and string_sufix == 'eps':
                    transition = 'eps'
                elif string_prefix == 'eps':
                    transition = string_sufix
                elif string_sufix == 'eps':
                    transition = string_prefix
                else:
                    transition = string_prefix + string_sufix
                
                if transition in automata.columns:
                    automata[transition][source] |= {destination}
                else:
                    col = pd.Series([""]*len(automata['type']), index=automata.index)
                    for i in range(0,len(col)):
                        col[i] = set()
                    automata.loc[:,transition] = col
                    automata[transition][source] |= {destination}
    automata = automata.drop(indexToRemove)
    for transition in automata.columns:
        if transition == 'type':
            continue
        for index, row in automata.iterrows():
            automata[transition][index].discard(indexToRemove)
        
    unused_columns = set()
    for column in automata.columns:
        if column == 'type':
            continue
        used = False
        for index, row in automata.iterrows():
            if len(automata[column][index]) > 0:
                used = True
                break
        if not used:
            unused_columns.add(column)
    for column in unused_columns:
        automata = automata.drop(column, axis=1)
        
    return automata

def automata_to_regex(automata):
    existing_symbols = set(automata.index)
    
    if 'eps' not in automata.columns:
        automata = automata.assign(eps=pd.Series([""]*len(automata['type'])).values)

    starts = set()
    ends = set()
    for index, _ in automata.iterrows():
        if automata['type'][index] == ">" or automata['type'][index] == ">*":
            starts |= {index}
        if automata['type'][index] == "*" or automata['type'][index] == ">*":
            ends |= {index}
        automata['type'][index] = ""
    
    data = [""] * len(automata.columns)
    
    q0 = generate_nonterminal(existing_symbols)
    row0 = pd.Series(data, index=automata.columns, name=q0)
    existing_symbols.add(q0)
    
    q1 = generate_nonterminal(existing_symbols)
    row1 = pd.Series(data, index=automata.columns, name=q1)
    existing_symbols.add(q1)
    
    automata = automata.append(row0)
    automata = automata.append(row1)
    
    automata['type'][q0] = ">"
    automata['type'][q1] = "*"
    
    for transition in automata.columns:
        if transition == 'type':
            continue
        for index, row in automata.iterrows():
            if isinstance(automata[transition][index], str):
                if automata[transition][index] == "":
                    automata[transition][index] = set()
                else:
                    automata[transition][index] = {automata[transition][index]}
    automata['eps'][q0] |= starts
    for end in ends:
        automata['eps'][end] |= {q1}
        
    while len(automata.index) > 2:
        automata = __remove_state(automata)
    
    starting_state = ""
    for index, row in automata.iterrows():
        if automata['type'][index] == ">" or automata['type'][index] == ">*":
            starting_state = index
            
    ending_state = ""
    for index, row in automata.iterrows():
        if automata['type'][index] == "*" or automata['type'][index] == ">*":
            ending_state = index
    
    transitions = []
    for transition in automata.columns:
        if transition == 'type':
            continue
        if len(automata[transition][starting_state]) == 1:
            if ending_state in automata[transition][starting_state]:
                transitions.append(transition)
            else:
                raise ValueError("Error")
        if len(automata[transition][ending_state]) > 0:
            raise ValueError("Error")
    
    return strip_parentheses(__concat_regex(transitions))
